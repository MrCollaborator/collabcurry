#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery flower \
    --app=celery_starter.celery \
    --url_prefix=flower \
    --debug=False
