Welcome to the Collab Curry documentation!
##########################################

Project Homepage: https://gitlab.com/bechtle-cisco-devnet/collaboration/collabcurry

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   setup
   rules
   environment
   modules

About
=====

------------
Collab Curry 
------------
Working the the Cisco Unified Routing XML Interface
-----------------------------------------------------------------------

This small python application running with `Flask`_ is accepting requests through Cisco UCMs [#f1]_ External Call Control (ECC) interface. Also known as `CURRI`_.
Incoming requests are then parsed and checked against a set of rules, provided through YAML files from the /rules directory. Depending on the rules, various actions can be started.
The following actions are currently implemented:

.. note::All possible ECC actions, see the `CURRI Documentation`_ for further information.

**Planned but not yet implemented actions**

* Caller Recognition against SQL, ...
* Out events through Syslog, SNMP, REST, ...



**Other Todos**

* Performance testing for design recommendations
* REST API for features like importing rules...

.. [#f1] Cisco Unified Communications Manager
.. _Flask: https://www.fullstackpython.com/flask.html
.. _CURRI: https://developer.cisco.com/site/curri/


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
