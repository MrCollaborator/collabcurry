import os
from app import create_app


# https://blog.miguelgrinberg.com/post/celery-and-the-flask-application-factory-pattern
app = create_app(os.getenv("FLASK_CONFIG") or "default")
app.app_context().push()
