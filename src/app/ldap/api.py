from flask import current_app as app
from app.common.helpers import check_object
from app.ldap.abstract import Ldap2Database
from app.ldap.service import LDAPService


class LdapImport:
    def __init__(self, backend: Ldap2Database) -> None:
        self.__inservice = False
        self.__ldap = None
        self.__backend = backend

        self.setup()

    @property
    def backend(self) -> Ldap2Database:
        return self.__backend

    @backend.setter
    def backend(self, backend: Ldap2Database) -> None:
        self.__backend = backend

    def delete_allusers(self) -> None:
        """Delete all userdata from the database backend."""
        self.__backend.drop()

    def import_users(self) -> None:
        if not self.__inservice:
            app.logger.info("LDAP Setup incomplete.")
            return None
        app.logger.info("Importing LDAP users.")
        app.logger.debug(f"  Searchfilter: {self.searchfilter}")
        app.logger.debug(f"  Searchbase: {self.searchbases}")
        app.logger.debug(f"  Searchattributes: {self.attributes}")

        result = self.__ldap.search(
            searchfilter=self.searchfilter,
            searchbases=self.searchbases,
            attributes=self.attributes,
        )
        if result:
            # Instead of merging add/deletes, just drop all current data.
            self.delete_allusers()
            for user in result:
                app.logger.debug(user)
                self.__backend.save_user(user)
            app.logger.info(f"  {len(result)} users imported.")
        else:
            app.logger.warning("  No users found.")

    def setup(self) -> None:
        """Setup the LDAP Service."""
        try:
            self.__ldap = LDAPService()

            self.attributes = app.config["LDAP_IMPORT_ATTRIBUTES"]
            self.searchbases = app.config["LDAP_IMPORT_SEARCHBASES"]
            self.searchfilter = app.config["LDAP_IMPORT_SEARCHFILTER"]

            check_object(self.attributes, list)
            check_object(self.searchbases, list)
            check_object(self.searchfilter, str)

            self.__inservice = True

        except TypeError as err:
            app.logger.warning("LDAP Setup incomplete, invalid or missing settings.")
            app.logger.debug(err)
