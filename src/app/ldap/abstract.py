"""Abstract layer for the LDAP2Database APIs."""
from abc import ABC, abstractmethod


class Ldap2Database(ABC):
    """Abstract class."""

    @abstractmethod
    def save_user(userdata: dict) -> dict:
        """Save data of an LDAP user to the database."""
        raise NotImplementedError

    @abstractmethod
    def get_users(*args) -> dict:
        """Get all LDAP users matching the provided arguments."""
        raise NotImplementedError
