"""Create a service object that is used for the LDAP connection.

The 'LDAPService' object is responsible for setting up a connection to
up to three LDAP servers. The class method 'search' performs an LDAP
query and returns a list of dictionaries as result.

If environment variables are used for the server IP/FQDN, up to three
servers can be used in a LDAP server pool, a feature of LDAP3.

If you provide a server with instantiation of the object, only this
single server will be used.
"""

import os
import functools
from typing import Optional, Union
from flask import current_app as app
from ldap3 import Connection, Server, ServerPool, FIRST
from ldap3.core.exceptions import LDAPBindError, LDAPExceptionError, LDAPSocketOpenError
from ldap3.utils.log import set_library_log_detail_level, BASIC
from app.common.helpers import check_object, match_hostaddress


class LDAPService(object):
    """LDAP Connection object."""

    def __init__(
        self,
        host: Optional[str] = None,
        port: Union[int, str, None] = None,
        use_ssl: Union[bool, str, None] = None,
        timeout: Union[int, str, None] = None,
        user: Optional[str] = None,
        password: Optional[str] = None,
    ):
        """
        Initialize the LDAPService.

        Args:
            host (str, optional): The FQDN or IP address of the server.
                Defaults to None.
            port ([int, str], optional]): The LDAP port to be used.
                Defaults to None.
            use_ssl ([bool, str], optional): Use SSL or not.
                Defaults to None.
            timeout ([int, str], optional): Timeout after which the
                connection fails. Defaults to None.
            user (str, optional): Username for the LDAP authentication.
                Defaults to None.
            password (str, optional): Password for the LDAP
                authentication. Defaults to None.
        """
        set_library_log_detail_level(BASIC)

        self.get_serverpool(host, port, use_ssl, timeout)

        self.user = user or os.environ.get("LDAP_USER", None)
        self.pwd = password or os.environ.get("LDAP_PW", None)

        check_object((self.user, self.pwd), str, allow_none=False)

        self.conn = None

    class Decorators(object):
        """Decorator class for the LDAP Service."""

        @staticmethod
        def ldap_setup(func):
            """Get a LDAP connection if there is none."""

            @functools.wraps(func)
            def check_connection(self, *args, **kwargs):
                if not self.conn:
                    try:
                        self.get_connection()
                    except Exception:
                        app.logger.critical(
                            "Operation failed, unable to connect to LDAP."
                        )
                        app.logger.debug("Traceback", exc_info=True)
                        return None
                return func(self, *args, **kwargs)

            return check_connection

    def get_connection(self) -> None:
        """Create and set a LDAP connection object as class attribute."""
        app.logger.info("Create LDAP bind to LDAP Server Pool.")

        try:
            app.logger.debug(f"  Binding user '{self.user}'")
            conn = Connection(
                self.ldapsrvpool, user=self.user, password=self.pwd, auto_bind=True
            )
            if conn.bind():
                app.logger.debug(f"  LDAP connection established. - {conn}")
                conn.unbind()
                self.conn = conn
                return
        except LDAPBindError as exc:
            app.logger.error(f"  Bind unsuccessful - {exc}")
        except LDAPSocketOpenError as exc:
            app.logger.error("  Connection Error. Please check server and LDAP paths.")
            app.logger.debug(exc)
        except LDAPExceptionError as exc:
            app.logger.warning(
                f"  Could not connect to server - "
                f"{conn.result}. "
                f"  Trying next server in list."
            )
            app.logger.debug(exc)

        raise Exception("Reached end of LDAP server list. Still no connection.")

    def get_serverpool(
        self,
        host: Union[str, None] = None,
        port: Union[int, str, None] = None,
        use_ssl: Union[bool, str, None] = None,
        timeout: Union[int, str, None] = None,
    ) -> None:
        """
        Create and set the LDAP server pool.

        A server pool with the three priorities 'PRIMARY', 'SECONDARY' and
        'TERTIARY' will only work when the environment variables are used.
        Using the class parameters allows only settings one server.

        Args:
            host (Union[str, None]): The FQDN or IP address of the server.
            port (Union[int, str, None]): The LDAP port to be used.
            use_ssl (Union[bool, str, None]): Use SSL or not.
            timeout (Union[int, str, None]): Timeout after which the connection
                fails.
        """
        check_object(host, str)
        check_object((port, timeout), (int, str))
        check_object(use_ssl, (bool, str))

        self.ldapsrvpool = ServerPool(None, pool_strategy=FIRST, active=True)

        for priority in ["PRIMARY", "SECONDARY", "TERTIARY"]:
            server = host or app.config[f"LDAP_HOST_{priority}"]
            port = port or app.config[f"LDAP_PORT_{priority}"]
            use_ssl = use_ssl or app.config[f"LDAP_SSL_{priority}"]
            timeout = timeout or app.config[f"LDAP_TIMEOUT_{priority}"]

            if not server:
                continue
            elif match_hostaddress(server) and port:
                ldapsrv = Server(
                    server, port=port, use_ssl=use_ssl, connect_timeout=timeout
                )
                self.ldapsrvpool.add(ldapsrv)
            else:
                raise ValueError("LDAP Hostaddress or Port incorrect.")

    @Decorators.ldap_setup
    def search(
        self,
        searchbases: list,
        searchfilter: Optional[str] = None,
        attributes: Optional[str] = None,
    ) -> list:
        """
        Execute a LDAP search.

        Args:
            searchfilter (str, optional): The LDAP search filter.
                Defaults to None.
            attributes (str, optional): LDAP attributes to be returned.
                Defaults to None.
            searchbases (list): A List of search bases the search is executed
                agains.

        Returns:
            list: Search results as a list of dictionaries.
        """
        app.logger.debug("Perform LDAP Search:")
        response = []

        self.conn.bind()
        for ou in searchbases:
            app.logger.debug(f"  {ou} - {searchfilter} - {attributes}")
            self.conn.search(ou, searchfilter, attributes=attributes)

            for item in self.conn.entries:
                ldapobj = {"dn": item.entry_dn}
                for a in item.entry_attributes:
                    ldapobj[a] = item[a].value

                response.append(ldapobj)
        self.conn.unbind()

        app.logger.debug("  Search Result:")
        for item in response:
            app.logger.debug(item)

        return response
