from flask import Blueprint
from flask_restx import Api
from app.ldap.routes import api as ldap


# Create the Blueprint for the LDAP module
bp = Blueprint('ldap', __name__)

api = Api(bp, title="CURRI LDAP API", version="1.0")

api.add_namespace(ldap)
