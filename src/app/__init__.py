import sys
import logging
from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix
from celery import Celery
from flask_mail import Mail
from flask_pymongo import PyMongo
from flask_redis import FlaskRedis
from config import configmap, baseconfig
from app.common.logger import CustomFormatter

celery = Celery(__name__, broker=baseconfig.CELERY_BROKER_URL)
mail = Mail()
mongo = PyMongo()
redis = FlaskRedis()


def create_app(configname="default"):
    """Create and configure the Flask app."""
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.config.from_object(configmap[configname])

    # celery.conf.update(app.config)
    celery.config_from_object(baseconfig, namespace="CELERY")
    celery.autodiscover_tasks(["app.tasks", "app.sendmail"])

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask

    mail.init_app(app)
    mongo.init_app(app)
    redis.init_app(app)

    handler = logging.StreamHandler(sys.stdout)
    formatter = CustomFormatter()
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    from app.api.v0 import bp as api_v0_bp

    app.register_blueprint(api_v0_bp)

    from app.curri import bp as ecc_bp

    app.register_blueprint(ecc_bp)

    from app.ldap import bp as ldap_bp

    app.register_blueprint(ldap_bp)

    return app
