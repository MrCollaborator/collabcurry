from typing import List, Optional
from pydantic import BaseModel, Field, validator


class EccRequestModel(BaseModel):
    """Model for the Request Data coming from the ECC request."""

    callingnumber: str = ""
    callednumber: str = ""
    transformedcgpn: str = ""
    transformedcdpn: str = ""
    triggerpointtype: str = ""


class EccDirectiveReject(BaseModel):
    """Model for the ECC response reject directive."""

    announcementid: str = None
    reason: str = None


class EccDirectiveDivert(BaseModel):
    """Model for the ECC response divert directive."""

    destination: str
    modify: dict = None
    resetcallhistory: str = None

    @validator("modify")
    def check_modify_scheme(cls, modifydict):
        allowed = ["callingnumber", "callednumber", "callingname", "calledname"]
        for key in modifydict.keys():
            if key not in allowed:
                raise ValueError(f"Key {key} not valid under 'modify'.")
        return modifydict

    @validator("resetcallhistory")
    def check_resetcallhistory_scheme(cls, rch):
        allowed = ["resetLastHop", "resetAllHops"]
        if rch not in allowed:
            raise ValueError(f"Key {rch} not valid under 'modify'.")
        return rch


class EccDirectiveContinue(BaseModel):
    """Model for the ECC response continue directive."""

    modify: dict = None
    greetingid: str = None

    @validator("modify")
    def check_modify_scheme(cls, modifydict):
        allowed = ["callingnumber", "callednumber", "callingname", "calledname"]
        for key in modifydict.keys():
            if key not in allowed:
                raise ValueError(f"Key {key} not valid under 'modify'.")
        return modifydict

    class Config:
        """Configuration for the 'EccDirectiveContinue' model."""

        extra = "ignore"


class EccDirectives(BaseModel):
    """Model for the ECC response directive base."""

    cont: Optional[EccDirectiveContinue] = Field(None, alias="continue")
    divert: EccDirectiveDivert = Field(None, alias="divert")
    reject: Optional[EccDirectiveReject] = Field(None, alias="reject")

    class Config:
        """Configuration for the 'EccDirectives' model."""

        extra = "ignore"


class EccActionCurri(BaseModel):
    """
    Model for the 'curri' action.

    The following attributes can be set:

        'decision' (mandatory)
        'status' (optional)
        'directives' (optional)

    All other attribute inputs will be ignored.
    """

    decision: str
    # Will not be checked further since only set through the application.
    status: Optional[dict]
    directives: Optional[EccDirectives]

    class Config:
        """Configuration for the 'EccActionCurri' model."""

        extra = "ignore"
        anystr_strip_whitespace = True


class EccActionLdap(BaseModel):
    """
    Model for the 'ldap' action.

    The following attributes can be set:

        'search_for' (mandatory)
        'callingname' (mandatory)

    All other attribute inputs will be ignored.
    """

    match_against: List[str]
    callingname: str

    class Config:
        """Configuration for the 'EccActionCurri' model."""

        extra = "ignore"
        anystr_strip_whitespace = True


class EccActionMail(BaseModel):
    """
    Model for the 'mail' action.

    The following attributes can be set:

        'active' (mandatory)
        'template_subject' (mandatory if 'active' is True)
        'template_body' (mandatory if 'active' is True)
        'mail_from' (mandatory if 'active' is True)
        'mail_to' (mandatory if 'active' is True)

    All other attribute inputs will be ignored.
    """

    active: bool = False
    subject: str = None
    body: str = None
    mail_from: str = None
    mail_to: List = None

    @validator("subject", "body", "mail_from", "mail_to")
    def mandatory_if_action_true(cls, value, values, field):
        if values["active"] and not value:
            raise ValueError(f"'{field.name}' has to be defined if 'active' is True.")
        return value

    class Config:
        """Configuration for the 'EccActionCurri' model."""

        extra = "ignore"
        anystr_strip_whitespace = True


class EccActionUccx(BaseModel):
    """
    Model for the 'uccx' action.

    The following attributes can be set:

        'ctiports' (mandatory)
        'agents' (mandatory)

    All other attribute inputs will be ignored.
    """

    ctiports: List[str]
    agents: List[str]

    class Config:
        """Configuration for the 'EccActionCurri' model."""

        extra = "ignore"
        anystr_strip_whitespace = True


class EccActions(BaseModel):
    """
    Model for the 'actions' configuration.

    The following attributes can be set:

        'mail' (optional)
        'curri' (optional)

    All other attribute inputs will be ignored.
    """

    curri: EccActionCurri
    ldap: Optional[EccActionLdap]
    mail: Optional[EccActionMail]
    uccx: Optional[EccActionUccx]

    class Config:
        """Configuration for the 'EccActions' model."""

        extra = "ignore"


class EccRule(BaseModel):
    """
    Model for the individual rule configurations.

    The following attributes can be set:

        'name' (mandatory)
        'conditions' (mandatory)
        'actions' (mandatory)

    All other attribute inputs will be ignored.
    """

    name: str
    conditions: dict
    actions: EccActions

    class Config:
        """Configuration for the 'EccRule' model."""

        extra = "ignore"


class EccRuleSet(BaseModel):
    """
    Model for the list of rule configurations.

    The Model expects a base attribute with the name 'rule' for each
    individual rule.

    Each rule is represented by the 'EccRule' model.

    All other attribute inputs will be ignored.
    """

    rule: EccRule

    class Config:
        """Configuration for the 'EccRuleSet' model."""

        extra = "ignore"


class EccRuleBase(BaseModel):
    """
    Base model for the imported rules YAML data

    The ruleset is based on model 'EccRuleSet'and. It expects a
    base attribute with the name 'ruleset'.

    All other attribute inputs will be ignored.
    """

    ruleset: List[EccRuleSet]

    class Config:
        """Configuration for the 'EccRuleBase' model."""

        extra = "ignore"
