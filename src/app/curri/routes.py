import traceback
from timeit import default_timer as timer
from flask import make_response, request
from flask import jsonify
from flask_restx import Namespace, Resource
from flask import current_app as app
from app.curri.facade import CurriHandler

api = Namespace("ecc", description="Collab Curry - ECC Module API")


@api.route("/request")
class curri_request_api(Resource):
    @api.response(200, "Success", headers={"Content-Type": "application/xml"})
    def post(self):
        """
        Takes the UCM ECC request and runs it against the CURRI API.

        Request URL: http://[server ip]:80/ecc/request

        ECC data has to be in the valid XML schema. Consult the documentation
        at https://developer.cisco.com for details.
        """
        start = timer()
        app.logger.info("ECC request received.")
        try:
            handler = CurriHandler()
            eccresult = handler.process_request(request.data)
            response = make_response(eccresult)
            # For this response we want to use XML as mediatype, Flask default is JSON.
            response.headers["Content-Type"] = "application/xml"
        except Exception as error:
            print(traceback.print_exc())
            result = {"success": False, "message": str(error)}
            return jsonify(result)

        end = timer()
        app.logger.debug(f"Elapsed time for 'curri_request_api': {end - start}")
        return response

    @api.response(200, "Success", headers={"Content-Type": "text/html"})
    def head(self):
        return "Ciao!"
