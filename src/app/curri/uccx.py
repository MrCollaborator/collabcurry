from flask import current_app as app
from app.common.helpers import check_pattern
from app.curri.cache import get_cache


def process_ccxrequest(action: dict, request: dict) -> dict:
    """Process incoming action and ecc requestdata regarding the CCX cache feature.

    Args:
        action (dict): Information about CTI Port and Agent directory numbers.
        request (dict): The original ECC request data as dictionary.

    Returns:
        dict: Result dictionary.
    """
    result = {
        'status': '',
        'clid': request['callingnumber'],
        'ctiport': '',
        'agent': ''
    }
    cache = get_cache('memcache')

    app.logger.debug(
        f"Checking if called number {request['callednumber']} is a CCX CTI Port")

    if check_toccx(request, action):
        data = {str(request['callednumber']): request['callingnumber']}
        cache.setdata(**data)
        result['status'] = 'cached'
        result['ctiport'] = request['callednumber']
        return result

    if check_fromccx(request, action) and check_toagent(request, action):
        result.update(check_cache(request, cache))

    app.logger.debug(f"UCCX Result: {result}")

    return result


def check_fromccx(request, action):
    return any(
        check_pattern(port, request['callingnumber'])
        for port in action['ctiports']
    )


def check_toagent(request, action):
    return any(
        check_pattern(port, request['callednumber'])
        for port in action['agents']
    )


def check_toccx(request, action):
    return any(
        check_pattern(port, request['callednumber'])
        for port in action['ctiports']
    )


def check_cache(request, cache):
    result = {
        'status': '',
        'ctiport': request['callingnumber'],
        'agent': ''
    }
    app.logger.debug(
        f"Retrieve cached data for CTI Port {request['callingnumber']}.")

    fromcache = cache.getdata(result['ctiport'])

    app.logger.debug(f"Cache Result: {fromcache}")

    if fromcache:
        result['status'] = 'retrieved'
        result['clid'] = fromcache[request['callingnumber']]
        result['ctiport'] = request['callingnumber']

    return result
