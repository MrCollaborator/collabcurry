from typing import List
from flask import current_app as app
from pymongo.collection import Collection
from app import mongo
from app.ldap.models import LdapUser

ldapusers: Collection = mongo.db.ldapusers


def process_dbrequest(action: dict, request: dict) -> dict:
    result = {
        'status': '',
        'clid': request['callingnumber'],
        'user': ''
    }

    response = query_db(ldapusers,
                        request['callingnumber'],
                        action['match_against'])

    if response:
        result['status'] = 'retrieved'
        result['user'] = response

    return result


def query_db(collection: Collection,
             callingnumber: str,
             attriblist: List[str]) -> dict:
    app.logger.debug("Querying MongoDB")

    for query in ({attribute: callingnumber} for attribute in attriblist):
        result = collection.find_one(query)
        print(result)
        if result:
            user = LdapUser.from_mongo(result)
            return user.dict()

    return None
