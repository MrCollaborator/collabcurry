"""
XML Parser Class for handling the Cisco UCM CURRI/ECC data.

The class can be
used for feeding the request and building the response data necessary for
ECC actions.

Check https://developer.cisco.com/site/curri/ for more information.

The CurriRequest class is using defusedxml for XML imports to minimize the
risk from XML vulnerabilities.
"""
from xml.dom import minidom
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, tostring
from defusedxml import ElementTree as dET
from flask import current_app as app
from app.common.helpers import check_object, prettyout
from app.curri.models import EccRequestModel


class EccRequest():
    """Data object for a CURRI Request."""

    def __init__(self):
        self.xmltree = None
        self.datamodel = EccRequestModel()

        self._currikeys = ['callingnumber',
                           'callednumber',
                           'transformedcgpn',
                           'transformedcdpn',
                           'triggerpointtype']

    def load(self, xmlstring: str):
        """
        Import XML data and run parse method.

        The ElementTree 'formstring' method is used to import the CURRI XML
        data and saved to the class object 'xmltree' which itself is an
        ElementTree class. The class method 'parse' is called to parse the
        data directly.

        Args:
            xmlstring (str): XML data from the CURRI request.
        """
        check_object(xmlstring, (str, bytes), allow_none=False)

        if isinstance(xmlstring, str):
            xmlstring.encode()

        self.xmltree = dET.fromstring(xmlstring)
        self.parse()

    def parse(self):
        """Parse through loaded XML data and extract the necessary infos."""
        for key in self._currikeys:
            searchstring = f".//*[@AttributeId='urn:Cisco:uc:1.0:{key}']"
            element = self.xmltree.find(searchstring)
            if element:
                setattr(self.datamodel, key, element[0].text)
            else:
                app.logger.debug(f"No value for {key} found.")


class EccReply():
    """Data object for the CURRI Reply."""

    def __init__(self):
        self.xmltree = None

        self._xmlroot = None
        self._xmlbody = None
        self._decision = None
        self._status = None
        self._obligation = None
        self._attributeassignment = None
        self._attributevalue = None
        self._cixml = None

        self.__build_basexml()

    @staticmethod
    def prettyxml(xmlnode):
        """Pretty printing of a provided xml 'Element' object."""
        xmldump = tostring(xmlnode)
        dom = minidom.parseString(xmldump)
        print(dom.toprettyxml())

    def __build_basexml(self):
        """Construct the base XML structure for the reply."""
        root = Element('Response')
        self._xmlroot = root

        body = SubElement(root, 'Result')
        body.set('ResourceId', 'CISCO:UC:VoiceOrVideoCall')
        self._xmlbody = body

        decision = SubElement(body, 'Decision')
        self._decision = decision

        status = SubElement(body, 'Status')
        statuscode = SubElement(status, 'StatusCode')
        statuscode.set('Value', '200')
        statusmessage = SubElement(status, 'StatusMessage')
        statusmessage.text = 'OK'
        statusdetail = SubElement(status, 'StatusDetail')
        statusdetail.text = 'HTTP response received successfully'
        self._status = status

        obligations = SubElement(body, 'Obligations')
        obligation = SubElement(obligations, 'Obligation')
        self._obligation = obligation

        attributeassignment = SubElement(obligation, 'AttributeAssignment')
        self._attributeassignment = attributeassignment

        attributevalue = SubElement(attributeassignment, 'AttributeValue')
        attributevalue.set('DataType',
                           'http://www.w3.org/2001/XMLSchema#string')
        self._attributevalue = attributevalue

    @staticmethod
    def __build_basedirective():
        """Add the base node for directives to the current xml tree."""
        app.logger.debug("Building CIXML base node.")
        cixml = Element('cixml')
        cixml.set('version', '1.0')

        return cixml

    def set_decision(self, decision):
        """
        Set the decision for this reply.

        Valid responses are: 'permit' or 'deny'. A permit automatically sets
        the 'Obligation' node parameters 'Fullfillon' and 'obligationId' as
        well as the AttributeAssignment paramete 'AttributeId'.

        Args:
            decision (str)
        """
        try:
            check_object(decision, str, allow_none=False)
        except TypeError as error:
            app.logger.warning(f"Problem with decision: {error}")
            return
        except Exception as error:
            app.logger.error(f"Unknown error: {error}")
            return

        decision = decision.lower()

        if decision in ['permit', 'deny',
                        'indeterminate', 'notapplicable']:
            # Obligations are overruled by the policy, we can just
            # leave it on 'Permit' at this point.
            self._obligation.set('FulfillOn', 'Permit')
            self._obligation.set('ObligationId', 'continue.simple')

            if decision == 'notapplicable':
                self._decision.text = 'NotApplicable'
            else:
                self._decision.text = decision.capitalize()
            if decision in ['permit', 'indeterminate']:
                self._attributeassignment.set('AttributeId',
                                              'Policy:continue.simple')
            if decision == 'deny':
                self._attributeassignment.set('AttributeId',
                                              'urn:cisco:xacml:is-resource')
        else:
            app.logger.error('Not a valid decision.')

    def set_status(self, code, message=None, detail=None):
        """
        Set the status for this reply.

        Valid responses are: 'permit' or 'deny'.

        Args:
            code (str): Status Code
            message (str): (optional) Status Message
            detail (str): (optional) Status Details
        """
        check_object(code, str, allow_none=False)
        check_object(message, str)
        check_object(detail, str)

        _statusbase = 'urn:oasis:names:tc:xacml:1.0:status:'
        _statuscodes = ['ok', 'missing-attribute',
                        'processing-error', 'syntax-error']

        if code.lower() in _statuscodes:
            statuscode = SubElement(self._status, 'StatusCode')
            statuscode.set('Value', f'{_statusbase}{code}')
        if message:
            statusmessage = SubElement(self._status, 'StatusMessage')
            statusmessage.text = message
        if detail:
            statusdetail = SubElement(self._status, 'StatusDetail')
            statusdetail.text = detail

    def set_directive(self, directive):
        """Set the directive part of the CURRI reply."""
        try:
            check_object(directive, dict)
        except TypeError as error:
            app.logger.warning(f"Input for directive incorrect: {error}")
            return
        except Exception as error:
            app.logger.error(f"Unknown error: {error}")
            return

        app.logger.debug("==> Dump Directive <==")
        prettyout(directive)

        def cont_directive(details):
            app.logger.debug("Adding 'continue' directive.")
            self.set_decision('permit')
            cont = SubElement(self._cixml, 'continue')
            if 'modify' in details and details['modify']:
                modify = SubElement(cont, 'modify')
                for key, value in details['modify'].items():
                    modify.set(key, value)
            if 'greetingid' in details and details['greetingid']:
                greeting = SubElement(cont, 'greeting')
                greeting.set('identification', details['greetingid'])

        def div_directive(details):
            app.logger.debug("Adding 'divert' directive.")
            self.set_decision('permit')
            divert = SubElement(self._cixml, 'divert')
            destination = SubElement(divert, 'destination')
            destination.text = details['destination']
            if 'modify' in details and details['modify']:
                modify = SubElement(divert, 'modify')
                for key, value in details['modify'].items():
                    modify.set(key, value)
            if 'resetcallhistory' in details and details['resetcallhistory']:
                rescallhistory = SubElement(divert, 'resetcallhistory')
                rescallhistory.text = details['resetcallhistory']

        def rej_directive(details):
            app.logger.debug("Adding 'reject' directive.")
            self.set_decision('deny')
            reject = SubElement(self._cixml, 'reject')
            if 'announcementid' in details and details['announcementid']:
                announce = SubElement(reject, 'announce')
                announce.set('identification', details['announcementid'])
            if 'reason' in details and details['reason']:
                reason = SubElement(reject, 'reason')
                reason.text = details['reason']

        self._cixml = self.__build_basedirective()

        # Hack caused by invalid declaration of 'continue' in pydantic model
        if directive['cont']:
            cont_directive(directive['cont'])
        elif directive['divert']:
            div_directive(directive['divert'])
        elif directive['reject']:
            rej_directive(directive['reject'])
        else:
            app.logger.warning("Directive tag exists but without content.")

        citext = ET.tostring(self._cixml, encoding='unicode', method='xml')
        self._attributevalue.text = str(citext)

    def load_action(self, actions):
        """
        Load a provided set of actions as dictionary and build XML reply.

        Args:
            actions (dict): Dictionary with decision and/or directives
                            information.
        """
        check_object(actions, dict, allow_none=False)

        if actions['directives']:
            self.set_directive(actions['directives'])
        else:
            app.logger.debug("No directives to be set.")

        try:
            self.set_decision(actions['decision'])
        except KeyError as err:
            app.logger.error("No decision provided which is mandatory", err)
            self.set_decision('permit')
            self.set_status('processing-error')
        return

    def exportxml(self):
        """
        Return the current CURRI reply in an UCM valid format.

        Returns:
            str: XML data for the CURRI interface as string.
        """
        xmldata = ET.tostring(self._xmlroot, encoding='unicode',
                              method='xml')
        xmldata = '<?xml version="1.0" encoding="UTF-8"?>' + xmldata
        return xmldata
