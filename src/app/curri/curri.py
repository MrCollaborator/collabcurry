import os
import yaml
from pydantic import ValidationError
from flask import current_app as app
from app.common.helpers import check_object, check_pattern, prettyout
from app.curri.ecc import EccRequest, EccReply
from app.curri.models import EccRuleBase


class CurriRules:
    """Class for importing and parsing a ruleset."""

    def __init__(self, filename="rules.yml"):
        check_object(filename, str, allow_none=False)

        self.default_path = "./rules"
        self.rulesfile = filename

        self.actions = {}
        self.request = None
        self.datamodel = None

        if self.load_ruleset(self.rulesfile):
            self.rulesdict = self.rules_list2dict()

    def get_rule(self, rulename) -> dict:
        """Return the requested rule configuration as dictionary."""
        check_object(rulename, str, allow_none=None)

        return self.rulesdict[rulename]

    def rules_list2dict(self):
        """Parse the currently imported rules into a dictionary."""
        ruleset = self.datamodel.dict()
        return {rule["rule"]["name"]: rule["rule"] for rule in ruleset["ruleset"]}

    def load_ruleset(self, filename: str, path: str = None) -> dict:
        """Load a ruleset directly from a YAML structured file.

        The default directory is ../rules which is usually mapped through
        the Docker container.

        Args:
            filename (str): Name of the file to be imported.
            path (str): (optional) Absolute path to the files directory.
        """
        check_object(filename, str, allow_none=False)
        check_object(path, str)

        if not path:
            localdir = os.path.dirname(os.path.abspath(__file__))
            rulesdir = os.path.abspath(os.path.join(localdir, self.default_path))

        rulesfile = os.path.abspath(os.path.join(rulesdir, filename))

        with open(rulesfile, "r") as file:
            try:
                yamlimport = yaml.safe_load(file)
                self.datamodel = EccRuleBase.parse_obj(yamlimport)
            except yaml.YAMLError as exception:
                app.logger.error(f"The provided file {file} is not valid.")
                app.logger.error(exception)
                return False
            except ValidationError as error:
                app.logger.error("Ruleset validation failed", error)
                app.logger.error(error)
                return False
        return True

    def get_ruleset(self, dataformat="json"):
        """
        Return the current ruleset.

        Args:
            dataformat (str): The output format of the data.
        """
        check_object(dataformat, str, allow_none=False)

        formats = ["json", "yaml"]

        if dataformat not in formats:
            raise NameError(
                f"'{dataformat}' is not a valid selection. "
                f"Valid output formats are {', '.join(formats)}."
            )

        if dataformat == "json":
            return self.datamodel.dict()

    def check_conditions(self, conditions: dict, requestdata: dict) -> bool:
        """Check a single rule from a ruleset against it's configuration.

        Args:
            conditions (dict): Rule conditions as dictionary.
            requestdata (dict): CURRI request information.

        Returns:
            bool: True if rule matches, else False.
        """
        app.logger.debug("Conditions Dump:")
        prettyout(conditions)
        try:
            for operator, values in conditions.items():
                if operator == "AND":
                    app.logger.debug(f"Values to check for operator AND: {values}")
                    shared = {
                        k: values[k]
                        for k in values
                        if k in requestdata and check_pattern(values[k], requestdata[k])
                    }
                    app.logger.debug(f"Post-check data: {shared}")
                    if len(shared) == len(values):
                        return True
                elif operator == "OR":
                    app.logger.debug(f"Values to check for operator OR: {values}")
                    shared = {
                        k: values[k]
                        for k in values
                        if k in requestdata and check_pattern(values[k], requestdata[k])
                    }
                    app.logger.debug(f"Post-check data: {shared}")
                    if shared:
                        return True
                else:
                    app.logger.debug("Not a valid operator. Expecting AND or OR.")
        except Exception:
            app.logger.error("Error while setting conditions.")
        return False

    def check_ruleset(self, requestdata: dict) -> str:
        """Run the currently loaded rules against the CURRI/ECC request.

        Args:
            requestdata (dict): Request information like 'callingnumer',
                                'callednumber'.

        Returns:
            str: Name of the matched rule. *None* if no match found.
                 The output rules actions are additionally stored in class
                 attribute '_actions' as dictionary.
        """
        check_object(requestdata, dict, allow_none=False)

        ruleset = self.datamodel.dict()

        app.logger.debug("==> DUMP Ruleset <==")
        prettyout(ruleset)
        app.logger.debug("==> DUMP Request Data <==")
        prettyout(requestdata)

        for rule in ruleset["ruleset"]:
            ruledict = rule["rule"]
            conditions = ruledict["conditions"]
            if self.check_conditions(conditions, requestdata):
                app.logger.info(f"Found match for rule '{ruledict['name']}'.")
                self.actions = ruledict["actions"]
                prettyout(self.actions)
                return ruledict["name"]
        app.logger.debug(f"No result for rule '{ruledict['name']}'.")
        return None

    def process_request(self, data: str) -> str:
        """Process XML input data from a CURRI POST request.

        Args:
            data (str): XML POST data coming from UCM

        Returns:
            str: XML reply as string object.
        """
        self.request = EccRequest()
        self.request.load(data)

        try:
            currirequest = self.request.datamodel.dict(by_alias=True)
            # print(f"PYDANTIC RESULT: {currirequest}")

            reply = EccReply()

            if self.check_ruleset(currirequest):
                app.logger.debug("==> Dump CURRI actions:")
                prettyout(self.actions["curri"])
                reply.load_action(self.actions["curri"])
            else:
                reply.set_decision("indeterminate")
        except Exception:
            app.logger.error("Error while processing ECC XML input.")
            reply.set_decision("indeterminate")
        return reply.exportxml()
