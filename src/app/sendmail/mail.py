from smtplib import SMTPRecipientsRefused
from typing import List
from flask import current_app as app
from flask_mail import Message
from app import mail
from app.common.helpers import template_or_str


def send_email(
    subject: str,
    sender: str,
    recipients: List,
    text_body: str,
    html_body: str = None,
    context: dict = None,
) -> bool:
    """Send an email.

    Args:
        subject (str):
            Subject line of the e-mail.
        sender (str):
            Senders e-mail address.
        recipients (List):
            Receipients e-mail addresses as list.
        text_body (str):
            Name of the template or string for the body. Both will be
            parsed with Jinja2, using the provided context.
        html_body (str, optional):
            Name of the template or string for the HTML body. Both will
            be parsed with Jinja2, using the provided context.
            Defaults to None.
        context (dict, optional):
            Context for parsing Jinja2 template variables.
            Defaults to {}.

    Returns:
        bool: Success of the e-mail delivery.
    """
    if not context:
        context = {}

    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = template_or_str(text_body, context)
    if html_body:
        msg.html = template_or_str(html_body, context)
    try:
        mail.send(msg)
        return True
    except ConnectionRefusedError as error:
        app.logger.error(f"Mail Delivery failed: {error}")
        return False
    except SMTPRecipientsRefused as error:
        app.logger.error(
            f"At least one recipient is not allowed by mailserver: {error}"
        )
        return False
    except Exception as error:
        app.logger.error(f"Send E-Mail - Unknown error: {error}")
        return False
