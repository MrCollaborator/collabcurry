from smtplib import SMTPRecipientsRefused
from flask import current_app as app
from flask_mail import Message
from app import celery
from app import mail
from app.common.helpers import template_or_str
from app.sendmail.mail import send_email


@celery.task
def send_email_async(
    subject, sender, recipients, text_body, html_body=None, context={}
):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = template_or_str(text_body, context)
    if html_body:
        msg.html = template_or_str(html_body, context)
    try:
        mail.send(msg)
    except ConnectionRefusedError as error:
        app.logger.error(f"Mail Delivery failed: {error}")
    except SMTPRecipientsRefused as error:
        app.logger.error(
            f"At least one recipient is not allowed by mailserver: {error}"
        )
    except Exception as error:
        app.logger.error(f"Send E-Mail - Unknown error: {error}")


@celery.task
def send_manyemails(number):
    for _ in range(1, number + 1):
        send_email.delay("test", "abc@def.uk", ["abc@def.uk"], "bodybody", "bodybody")
