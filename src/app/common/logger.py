import logging


class CustomFormatter(logging.Formatter):
    """Set custom formatting per Loglevel.

    Source: https://stackoverflow.com/a/14859558

    Returns:
        [type]: [description]
    """

    debug_fmt = "[%(filename)s:%(lineno)s - %(module)s:%(funcName)20s() ] %(msg)s"

    def __init__(self):
        super().__init__(fmt="%(levelno)d: %(msg)s",
                         datefmt=None,
                         style='%')

    def format(self, record):

        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        if record.levelno == logging.DEBUG:
            self._style._fmt = CustomFormatter.debug_fmt

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig

        return result
