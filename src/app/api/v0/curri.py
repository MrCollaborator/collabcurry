"""API for the Collabcurry application.

Currently just with a bare minimum and work in progress.

Implemented endpoints:

/version
    Returns API version
/ruleset/
    Returns the currently imported ruleset.
"""
from flask import jsonify
from flask_restx import Namespace, Resource
from flask import current_app as app
from app.curri.curri import CurriRules

api = Namespace("curri", description="Collab Curry - CURRI REST API")

Rules = CurriRules()


@api.route("")
@api.route("/")
@api.route("/version")
class curri_get_version_api(Resource):
    def get(self):
        app.logger.debug("Request on base url.")
        return {"version": "0.1"}

    def post(self):
        app.logger.debug("Request on base url.")
        return {"version": "0.1"}

    def head(self):
        app.logger.debug("Request on base url.")
        return "Gruezi!"


@api.route("/ruleset/")
class curri_rulesets_api(Resource):
    def get(self):
        """
        Returns the currently imported ruleset.
        """
        try:
            ruleset = Rules.get_ruleset()
        except Exception as error:
            result = {"success": False, "message": str(error)}
            return jsonify(result)
        result_api = {"success": True, "result": ruleset}
        return jsonify(result_api)
