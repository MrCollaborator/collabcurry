***This project is still work in progress and running in test environments. Use carefully!***

# About

A Flask based app for rule based handling of Cisco UCMs ECC/CURRI requests.

## Documentation

See the documentation at: <https://collabcurry.readthedocs.io/en/latest/>